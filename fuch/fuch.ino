struct segmentPinout {
  int T;
  int TL; 
  int TR;
  int M;
  int BR;
  int Dot;
  int B; 
  int BL;
  } segmentPins = {9, 8, 2, 7, 3, 4, 5, 6};

int digitPinout[4] = {10, 11, 12, 13};

struct charactermap {
  int F[8];
  int U[8];
  int C[8];
  int H[8];
  
} characters = {
  {segmentPins.T, segmentPins.TL, segmentPins.M, segmentPins.BL},
  {segmentPins.TL, segmentPins.TR, segmentPins.BL, segmentPins.BR, segmentPins.B, 0, 0, 0},
  {segmentPins.T, segmentPins.TL, segmentPins.BL, segmentPins.B, 0, 0, 0, 0},
  {segmentPins.TR, segmentPins.TL, segmentPins.M, segmentPins.BL, segmentPins.BR, 0, 0, 0}
};

int currentdigit = 0;
int *FUCH[4] = {
  characters.F, characters.U, characters.C, characters.H
};

void setup() {

  for (int i=2; i<=13; i++){
    pinMode(i, OUTPUT);
  }
  for (int i=0; i<=3; i++)
  {
    digitalWrite(digitPinout[i], HIGH);
  }
}

void loop() {
  digitalWrite(digitPinout[currentdigit], LOW);
  for (int i=0; i<=7; i++)
  {
    digitalWrite(FUCH[currentdigit][i], HIGH);

  };
  delay(1000); 
    for (int i=0; i<=7; i++)
  {
    digitalWrite(FUCH[currentdigit][i], LOW);

  };
  digitalWrite(digitPinout[currentdigit], HIGH);
  currentdigit = ++currentdigit % 4;
}
