int PiezoPin = 10;
int EchoPin = 7;
int TriggerPin = 13;

void setup() {
  Serial.begin(115200);
  pinMode(PiezoPin, OUTPUT);
  pinMode(EchoPin, INPUT);
  pinMode(TriggerPin, OUTPUT);
}

void loop() {
  float duration, distance;
  unsigned int frequency;
  digitalWrite(TriggerPin, LOW);
  delayMicroseconds(2);
  digitalWrite(TriggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(TriggerPin, LOW);
  duration = pulseIn(EchoPin, HIGH);
  Serial.print("duration: ");
  Serial.println(duration);
  distance = calculateRange(duration);
  Serial.print("distance: ");
  Serial.println(distance);
  //tone(PiezoPin, distanceToFrequency(distance));
  int bpm = 120; //distanceToBpm(distance);
  unsigned long beat = (unsigned long) (60.0 * 1000.0 / (float) bpm);
  unsigned long w = 4 * beat;
  unsigned long h = (unsigned long) w / 2;
  unsigned long q = (unsigned long) w / 4;
  unsigned long e = (unsigned long) w / 8;
  unsigned long s = (unsigned long) w / 16;
  unsigned long t = (unsigned long) q / 3;
  unsigned long a = 440;
  unsigned long b = 494;
  unsigned long f = 349;
  unsigned long c = 523;
  unsigned long d = 587;
  unsigned long e1 = 659;
  unsigned long f2 = 698;
  unsigned long g = 784;
  unsigned long a2 = 880;
  unsigned long b2 = 988;
  
  tone(PiezoPin, flat(b)); delay(q);
  tone(PiezoPin, f); delay(dotted(q));
  tone(PiezoPin, flat(b)); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, flat(b)); delay(s);
  tone(PiezoPin, c); delay(s);
  tone(PiezoPin, d); delay(s);
  tone(PiezoPin, flat(e1)); delay(s);
  tone(PiezoPin, f2); delay(5 * e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f2); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f2); delay(t);
  tone(PiezoPin, flat(g)); delay(t);
  tone(PiezoPin, flat(a2)); delay(t);
  tone(PiezoPin, flat(b2)); delay(5 * e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, flat(b2)); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, flat(b2)); delay(t);
  tone(PiezoPin, flat(a2)); delay(t);
  tone(PiezoPin, flat(g)); delay(t);
  tone(PiezoPin, flat(a2)); delay(dotted(e));
  tone(PiezoPin, flat(g)); delay(s);
  tone(PiezoPin, f2); delay(h - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f2); delay(q);
  tone(PiezoPin,flat(e1)); delay(e);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin,flat(e1)); delay(s);
  tone(PiezoPin, f2); delay(s);
  tone(PiezoPin, flat(g)); delay(h);
  tone(PiezoPin, f2); delay(e);
  tone(PiezoPin,flat(e1)); delay(e);
  tone(PiezoPin,flat(d)); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin,flat(d)); delay(s);
  tone(PiezoPin,flat(e1)); delay(s);
  tone(PiezoPin, f2); delay(h);
  tone(PiezoPin,flat(e1)); delay(e);
  tone(PiezoPin,flat(d)); delay(e);
  tone(PiezoPin, c); delay(e-50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, c); delay(s);
  tone(PiezoPin, d); delay(s);
  tone(PiezoPin, e1); delay(h);
  tone(PiezoPin, g); delay(q);
  tone(PiezoPin, f2); delay(e);
  tone(PiezoPin, f); delay(s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay (s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay (s - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(e - 50);
  noTone(PiezoPin); delay(50);
  tone(PiezoPin, f); delay(e);
  noTone(PiezoPin);
  delay(1000);
}

unsigned long flat(unsigned long frequency) {
  return (unsigned long) ((float) frequency) / 1.059;
}
unsigned long sharp(unsigned long frequency) {
  return (unsigned long) ((float) frequency) * 1.059;
}
unsigned long dotted(unsigned long duration) {
  return duration * 1.5;
}

float calculateRange(float duration) {
  float soundVelocity = 343.0 / 10000;
  return (duration * soundVelocity) / 2;
}

unsigned int distanceToFrequency(float distance) {
  return (unsigned int) ((distance - 2.0) * 11.56) + 31;
}

int distanceToBpm(float distance) {
  return (int) (distance * 2.9) + 40;
}